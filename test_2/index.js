"use strict";

/**
 * The `test_data.json` contains an object array of "The Simpsons" characters and their catchphrase. This function
 * will add a new property `example` to each object that is a combining the `first_name`, `last_name`, and
 * `catchphrase` properties into a new sentence. The function should then only return the elements where
 * the last name is "Simpson".
 *
 * The new sentence should read like "Homer Simpson says Doh!".  See the `expected_output.json` file for the
 * expected results of this.
 *
 * @returns  {Object[]}
 */
module.exports = function test2() {
	const test1 = require("../test_1/index");
	const test1Results = test1(); 

	let results = [];

	for (let i = 0; i < test1Results.length; i++) {

		// I decided to convert the last name to lower case for the comparison so that the code isn't dependent on case sensitivity.
		if (test1Results[i]["last_name"].toLowerCase() == "simpson")
			results.push(test1Results[i]);
	}

	return results;
};
