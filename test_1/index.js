"use strict";

/**
 * The `test_data.json` contains an object array of "The Simpsons" characters and their catchphrase. This function
 * will add a new property `example` to each object that is a combining the `first_name`, `last_name`, and
 * `catchphrase` properties into a new sentence.
 *
 * The new sentence should read like "Homer Simpson says Doh!".  See the `expected_output.json` file for the
 * expected results of this.
 *
 * @returns  {Object[]}
 */
module.exports = function test1() {
	const fs = require("fs");
	const path = require("path");
	
	let results = JSON.parse(fs.readFileSync(path.resolve(__dirname, "./test_data.json")));

	for (let i = 0; i < results.length; i++) {

		/**
		 * I took reference of template strings at https://stackoverflow.com/questions/610406/javascript-equivalent-to-printf-string-format.
		 *
		 * Even though template strings are found in ES6 and not in previous versions, I assume that it's valid to use and not have to cater to browsers
		 * that use ES5 or below since 'let' is introduced in ES6 and is written here as part of the base code.
		 */ 
		results[i]["example"] = `${results[i]["first_name"]} ${results[i]["last_name"]} says ${results[i]["catchphrase"]}`;

	}

	return results;
};
